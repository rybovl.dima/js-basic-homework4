// 1
function isNumber(value) {
    return typeof value === 'number' && !isNaN(value);
}

let num1;
do {
    num1 = parseFloat(prompt("Введіть перше число:"));
} while (!isNumber(num1));

let num2;
do {
    num2 = parseFloat(prompt("Введіть друге число:"));
} while (!isNumber(num2));

let smaller = Math.min(num1, num2);
let larger = Math.max(num1, num2);

console.log(`Числа від ${smaller} до ${larger}:`);
for (let i = smaller; i <= larger; i++) {
    console.log(i);
}

// 2
let number;

do {
    number = parseInt(prompt("Введіть число:"));
} while (isNaN(number) || number % 2 !== 0);

console.log(`Введене число ${number} є парним.`);